FROM php:8.0.17-cli-alpine3.15

RUN apk update \
    && apk add unzip git php8-phpdbg \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
