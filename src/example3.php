<?php

class Task {

    private $taskId;
    private $coroutine;
    private $sendValue = null;
    private $beforeFirstYield = true;

    /**
     * @param int       $taskId
     * @param Generator $coroutine
     */
    public function __construct($taskId, Generator $coroutine) {
        $this->taskId = $taskId;
        $this->coroutine = $coroutine;
    }

    /**
     * @return int
     */
    public function getTaskId() {
        return $this->taskId;
    }

    /**
     * @param mixed $sendValue
     */
    public function setSendValue($sendValue) {
        $this->sendValue = $sendValue;
    }

    /**
     * @return mixed
     */
    public function run() {
        if ($this->beforeFirstYield) {
            $this->beforeFirstYield = false;
            return $this->coroutine->current();
        } else {
            $retval = $this->coroutine->send($this->sendValue);
            return $retval;
        }
    }
    
    /**
     * return bool
     */
    public function isFinished() {
        return !$this->coroutine->valid();
    }
}

class Scheduler {

    private $maxTaskId = 0;
    private $taskMap = [];
    private $taskQueue;

    public function __construct() {
        $this->taskQueue = new SplQueue();
    }

    /**
     * @param Generator $coroutine
     *
     * @return int
     */
    public function newTask(Generator $coroutine) {
        $tid = ++$this->maxTaskId;
        $task = new Task($tid, $coroutine);
        $this->taskMap[$tid] = $task;
        $this->schedule($task);
        return $tid;
    }

    /**
     * @param int $tid
     *
     * @return bool
     */
    public function killTask($tid) {
        if (!isset($this->taskMap[$tid])) {
            return false;
        }
        unset($this->taskMap[$tid]);
        foreach ($this->taskQueue as $i => $task) {
            if ($tid === $task->getTaskId()) {
                unset($this->taskQueue[$i]);
                break;
            }
        }
        return true;
    }

    /**
     * @param Task $task
     */
    public function schedule(Task $task) {
        $this->taskQueue->enqueue($task);
    }

    /**
     * @return void
     */
    public function run() {
        while (!$this->taskQueue->isEmpty()) {
            $task = $this->taskQueue->dequeue();
            $retval = $task->run();
            if ($retval instanceof SystemCall) {
                $retval($task, $this);
                continue;
            }
            if ($task->isFinished()) {
                unset($this->taskMap[$task->getTaskId()]);
            } else {
                $this->schedule($task);
            }
        }
    }
}

class SystemCall {
    
    private $callback;
    
    /**
     * @param callable $callback
     */
    public function __construct(callable $callback) {
        $this->callback = $callback;
    }
    
    /**
     * @param Task      $task
     * @param Scheduler $scheduler
     */
    public function __invoke(Task $task, Scheduler $scheduler) {
        $callback = $this->callback;
        return $callback($task, $scheduler);
    }
}

function getTaskId() {
    return new SystemCall(function(Task $task, Scheduler $scheduler) {
        $task->setSendValue($task->getTaskId());
        $scheduler->schedule($task);
    });
}

function newTask(Generator $coroutine) {
    return new SystemCall(function(Task $task, Scheduler $sheduler) use ($coroutine) {
        $task->setSendValue($scheduler->newTask($coroutine));
        $scheduler->schedule($task);
    });
}

function killTask($tid) {
    return new SystemCall(function(Task $task, Scheduler $scheduler) use ($tid) {
        $task->setSendValue($sсheduler->killTask($tid));
        $scheduler->schedule($task);
    });
};

function childTask() {
    $tid = (yield getTaskId());
    while (true) {
        echo "Child task $tid still alive!\n";
        yield;
    }
}

function task($max) {
    $tid = (yield getTaskId()); // Здесь syscall
    for ($i = 1; $i <= $max; ++$i) {
        echo "This is task $tid iteration $i.\n";
        yield;
    }
}

$scheduler = new Scheduler();

$scheduler->newTask(task(10));
$scheduler->newTask(task(5));

$scheduler->run();
