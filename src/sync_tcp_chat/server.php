<?php

$uri = 'tcp://0.0.0.0:8001';

$flags = STREAM_SERVER_BIND | STREAM_SERVER_LISTEN;

$serverSocket = @stream_socket_server($uri, $errno, $errstr, $flags);

if (!$serverSocket || $errno) {
    die(sprintf("Could not create server: %s [Errno: #%d]", $uri, $errno, $errstr));
}

while ($client = stream_socket_accept($serverSocket, -1)) {
    while (($line = fgets($client)) !== false) {
        fputs($client, $line);
    }
}

