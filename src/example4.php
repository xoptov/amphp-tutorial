<?php

function gen() {
    yield 'foo';
    yield 'bar';
}

$gen = gen();
$value = $gen->send(1);
echo $value.PHP_EOL;
