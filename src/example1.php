<?php

require_once dirname(__DIR__).'/vendor/autoload.php';

function gen() {
    echo '---Before 1---'.PHP_EOL;
    yield 1;
    echo '---Before 2---'.PHP_EOL;
    yield 2;
    echo '---Before 3---'.PHP_EOL;
    yield 3;
}

$gen = gen(); // Это возвращается генератор.

//$gen->rewind(); // Запустит код до первого yield.

//$value = $gen->current(); // Запустит код до текущего yield и вернёт то что правее от yield.

//echo 'value='.$value.PHP_EOL;

$gen->next(); // Запустит код до 2-го yield.

//$value = $gen->current();
//echo 'value='.$value.PHP_EOL;
//$gen->next();

//$value = $gen->current();
//echo 'value='.$value.PHP_EOL;
//$gen->next();
