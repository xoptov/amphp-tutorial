<?php

require_once dirname(__DIR__).'/../vendor/autoload.php';

use Amp\Loop;
use Amp\Socket\Socket;
use Amp\Socket\Server;
use function Amp\asyncCall;

Loop::run(function() {
    
    $uri = 'tcp://0.0.0.0:8001';
    
    $clientHandler = function(Socket $socket) {
        while(null !== $chunk = yield $socket->read()) {
            yield $socket->write($chunk);
        }
    };
    
    $server = Server::listen($uri);
    
    while ($socket = yield $server->accept()) {
        asyncCall($clientHandler, $socket);
    }
});

