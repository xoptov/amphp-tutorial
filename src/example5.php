<?php

function test1() {
    return 'some value';    
}

function test2() {
    $value = (yield test1());
    echo $value.PHP_EOL;
}

$gen = test2();
$gen->next();
