<?php

function println() {
    while (true) {
        echo 'Before yield'.PHP_EOL;
        echo (yield 1).PHP_EOL;
        echo 'Tick!'.PHP_EOL;
    }
}

$println = println();
//$println->rewind();
//$value = $println->current();
//echo $value.PHP_EOL;
//$println->next();
$println->send('Hello, World!');
//$println->send('I\'m software developer!!!');
